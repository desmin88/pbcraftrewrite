package net.paintballcraft.plugin;

import net.minecraft.server.Packet20NamedEntitySpawn;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.packet.listener.PacketListener;
import org.getspout.spoutapi.packet.standard.MCPacket;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class PacketModification implements PacketListener {
    @Override
    public boolean checkPacket(Player player, MCPacket mcPacket) {
        Packet20NamedEntitySpawn packet20 = (Packet20NamedEntitySpawn) mcPacket.getPacket();
        System.out.println("0");
        System.out.println("checkPacket - checking to see if player inside packet is playing the game");
        // --The person inside the packet is playing in a game
        if(GameInstance.getInstance().isPlaying(packet20.b)) {
            System.out.println("1");
            // --Get name with team color, strip it length 16 max if applicable--
            String nameWithColor = GameInstance.getInstance().getTeamColor(packet20.b) + packet20.b;
            if(nameWithColor.length() > 16)
                nameWithColor = nameWithColor.substring(0, 16);

            SpoutPlayer spoutPlayer = (SpoutPlayer) player;
            // --The player received the packet is a spout craft enabled player
            if(spoutPlayer.isSpoutCraftEnabled()) {
                System.out.println("2");
                  // --Send the receiving player the packet immediately, set up a delay setTitleFor as well, and then finally return false so we dont send the packet twice--
                  SpoutPlayer inPacket = (SpoutPlayer) Bukkit.getPlayer(packet20.b);
                  spoutPlayer.sendImmediatePacket(mcPacket);
                  this.setTitleFor(inPacket, spoutPlayer, nameWithColor);
                  return false;

            } else {
                 // --Set the name to include color--
                System.out.println("3");
                System.out.println(nameWithColor);
                packet20.b = nameWithColor;
                return true;

            }

        }

        return true;
    }


     private void setTitleFor(final SpoutPlayer inPacket, final SpoutPlayer spoutPlayer, final String nameWithColor) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(PBCraft.getPBCraft(), new Runnable() {
            @Override
            public void run() {
                inPacket.setTitleFor(spoutPlayer, nameWithColor);
            }
        }, 5);
     }
}
