package net.paintballcraft.plugin.database;

import net.paintballcraft.plugin.PBCraft;

public class PlayerDBBuffer {

    public PlayerDBBuffer(String name) {
        this.name = name;
        this.read();
    }
    
    private String name = "";
    
    private int points = 0;
    private int speed = 0;
    private int jump = 0;
    private int eggs = 0;
    private int snowballs = 0;
    private int rank = 1;
    private int kills = 0;
    
    public synchronized int getRank() {
        return rank;
    }

    public synchronized void setRank(int rank) {
        this.rank = rank;
    }

    public synchronized boolean isHide() {
        return isHide;
    }

    public synchronized void setHide(boolean isHide) {
        this.isHide = isHide;
    }

    private boolean donor = false;
    private boolean isHide = false;
    
    public synchronized void write() {
        DatabaseLogger dlg = PBCraft.getPBCraft().getDB();
        dlg.writePlayer(this);
        
        /* OLD METHOD
        dlg.setPoints(this.name, this.points);
        dlg.setSpeed(this.name, this.speed);
        dlg.setJump(this.name, this.jump);
        dlg.setEggs(this.name, this.eggs);
        dlg.setSnowballs(this.name, this.snowballs);
        dlg.setRank(this.name, this.rank);
        dlg.setLastPlayed(this.name);
        dlg.setKills(this.name, this.kills);
        
        //dlg.setDonor(this.name, this.donor);
        dlg.setHidemessage(this.name, this.isHide);
        
        */     
    }
    
    public synchronized void read() {
        DatabaseLogger dlg = PBCraft.getPBCraft().getDB();
        dlg.readPlayer(this);
        
        /* OLD METHOD
        this.points = dlg.getPoints(this.name);
        this.speed = dlg.getSpeed(this.name);
        this.jump = dlg.getJump(this.name);
        this.eggs = dlg.getEggs(this.name);
        this.snowballs = dlg.getSnowballs(this.name);
        this.rank = dlg.getRank(this.name);
        this.kills = dlg.getKills(this.name);
        this.donor = dlg.isDonor(this.name);
        this.isHide = dlg.isHidemessage(this.name);
        */
    }

    public synchronized void addPoints(int p) {
        this.points = points + p;
    }
    
    public synchronized String getName() {
        return name;
    }

    public synchronized void setName(String name) {
        this.name = name;
    }

    public synchronized int getPoints() {
        return points;
    }

    public synchronized void setPoints(int points) {
        this.points = points;
    }

    public synchronized int getSpeed() {
        return speed;
    }

    public synchronized void setSpeed(int speed) {
        this.speed = speed;
    }

    public synchronized int getJump() {
        return jump;
    }

    public synchronized void setJump(int jump) {
        this.jump = jump;
    }

    public synchronized int getEggs() {
        return eggs;
    }

    public synchronized void setEggs(int eggs) {
        this.eggs = eggs;
    }

    public synchronized int getSnowballs() {
        return snowballs;
    }

    public synchronized void setSnowballs(int snowballs) {
        this.snowballs = snowballs;
    }

    public synchronized boolean isDonor() {
        return donor;
    }

    public synchronized void setDonor(boolean donor) {
        this.donor = donor;
    }

	public synchronized int getKills() {
		return kills;
	}

	public synchronized void setKills(int kills) {
		this.kills = kills;
	}
	
	public synchronized void addKills(int kills) {
		this.kills += kills;
	}

    
}
