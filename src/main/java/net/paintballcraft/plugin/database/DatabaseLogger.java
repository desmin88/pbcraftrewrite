package net.paintballcraft.plugin.database;

import lib.PatPeter.SQLibrary.MySQL;
import net.paintballcraft.plugin.PBCraft;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class DatabaseLogger {
    private MySQL mysql;

    public DatabaseLogger(String hostname, String username, String password, String database) {
        // Test info:
        // Host: mysql.tespia.org
        // User: pb_readwrite
        // Pass: starchaser
        // DB: pbcraft_game
        //public MySQL(Logger log, String prefix, String hostname, String portnmbr, String database, String username, String password) {
        this.mysql = new MySQL(PBCraft.getPBCraft().getLogger(), "pb_", hostname, "3306", database, username, password);

        this.Open();
    }

    public void Close() {
        this.mysql.close();
    }

    public void Open() {
        try {
            this.mysql.open();

        } catch (Exception e) {
            PBCraft.getPBCraft().getLogger().info(e.getMessage());
        }
    }

    public void CheckOnline() {
        try {
            if (!this.mysql.checkConnection() || this.mysql.getConnection().isClosed()) {
                this.Open();
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            this.Open();
        }
    }

    public int getPoints(String name) {
        return getInt(name, "points");
    }

    public void setPoints(String name, int points) {
        setInt(name, "points", points);
    }

    public void addPoints(String name, int points) {
        setInt(name, "points", getInt(name, "points") + points);
    }

    public int getSpeed(String name) {
        return getInt(name, "speed");
    }

    public void setSpeed(String name, int speed) {
        setInt(name, "speed", speed);
    }

    public int getJump(String name) {
        return getInt(name, "jump");
    }

    public void setJump(String name, int jump) {
        setInt(name, "jump", jump);
    }

    public int getEggs(String name) {
        return getInt(name, "eggs");
    }

    public void setEggs(String name, int eggs) {
        setInt(name, "eggs", eggs);
    }

    public int getRank(String name) {
        return getInt(name, "rank");
    }

    public void setRank(String name, int rank) {
        setInt(name, "rank", rank);
    }

    public int getSnowballs(String name) {
        return getInt(name, "snowballs");
    }

    public void setSnowballs(String name, int snowballs) {
        setInt(name, "snowballs", snowballs);
    }

    public int getKills(String name) {
        return getInt(name, "kills");
    }

    public void setKills(String name, int kills) {
        setInt(name, "kills", kills);
    }

    public boolean isDonor(String name) {
        this.CheckOnline();

        int userID = getUserID(name);

        if(userID == 0)
            return false;

        ResultSet rs = mysql.query("SELECT donor_level FROM hc_donors WHERE user_id = '" + getUserID(name) + "'");
        try {
            if (rs.next()) {
                int level = rs.getInt("donor_level");
                return level >= 3;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean isHidemessage(String name) {
        return getBoolean(name, "hidemsg");
    }

    public void setHidemessage(String name, boolean hidemessage) {
        setBoolean(name, "hidemsg", hidemessage);
    }

    public int getUserID(String name) {
        this.CheckOnline();

        checkUserExists(name);

        ResultSet rs = mysql.query("SELECT field_value FROM xf_user_field_value WHERE field_id = 'minecraft' AND field_value = '" + name + "'");
        try {
            if (rs.next()) {
                return rs.getInt("field_value");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int getLastPlayed(String name)
    {
        return getInt(name, "lastplayed");
    }

    public void setLastPlayed(String name)
    {
        this.CheckOnline();

        checkUserExists(name);
        mysql.query("UPDATE `pb_players` SET lastplayed = UNIX_TIMESTAMP() WHERE `mcname` = '" + name + "'");
    }

    public boolean readPlayer(PlayerDBBuffer player)
    {
        this.CheckOnline();

        checkUserExists(player.getName());

        ResultSet rs = mysql.query("SELECT * FROM pb_players WHERE mcname = '" + player.getName() + "'");
        try {
            if (rs.next()) {
                player.setEggs(rs.getInt("eggs"));
                player.setHide(rs.getBoolean("hidemsg"));
                player.setJump(rs.getInt("jump"));
                player.setPoints(rs.getInt("points"));
                player.setKills(rs.getInt("kills"));
                player.setRank(rs.getInt("rank"));
                player.setSpeed(rs.getInt("speed"));
                player.setSnowballs(rs.getInt("snowballs"));
                player.setDonor(this.isDonor(player.getName()));

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void writePlayer(PlayerDBBuffer player) {
        this.CheckOnline();
        checkUserExists(player.getName());

        int hide = 0;

        if(player.isHide())
            hide = 1;

        mysql.query("UPDATE `pb_players` SET `points`= " + player.getPoints() +
                ", `kills`= " + player.getKills() +
                ", `speed`= " + player.getSpeed() +
                ", `jump`= " + player.getJump() +
                ", `eggs`= " + player.getEggs() +
                ", `rank`= " + player.getRank() +
                ", `snowballs`= " + player.getSnowballs() +
                ", `hidemsg`= " + hide +
                " WHERE `mcname`='" + player.getName() +"'");

        this.setLastPlayed(player.getName());
    }

    // Checks if user exists in DB. If not, creates an entry.
    // Returns if the user was initially in DB.
    private boolean checkUserExists(String mcname) {
        this.CheckOnline();

        ResultSet rs = mysql.query("SELECT user_id FROM pb_players WHERE mcname = '" + mcname + "'");
        try {
            if (rs.next()) {
                return true;
            } else {
                mysql.query("INSERT INTO `pb_players` (`mcname`) VALUES('" + mcname + "')");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private int getInt(String name, String col) {
        this.CheckOnline();

        checkUserExists(name);

        ResultSet rs = mysql.query("SELECT " + col + " FROM pb_players WHERE mcname = '" + name + "'");
        try {
            if (rs.next()) {
                return rs.getInt(col);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    private void setInt(String name, String col, int val) {
        this.CheckOnline();

        checkUserExists(name);
        mysql.query("UPDATE `pb_players` SET `" + col + "`=" + val + " WHERE `mcname` = '" + name + "'");
    }

    private void setBoolean(String name, String col, boolean val) {
        this.CheckOnline();

        checkUserExists(name);

        int value = 0;
        if (val)
            value++;

        mysql.query("UPDATE `pb_players` SET `" + col + "`=" + value + " WHERE `mcname` = '" + name + "'");
    }

    private boolean getBoolean(String name, String col) {
        this.CheckOnline();

        checkUserExists(name);

        ResultSet rs = mysql.query("SELECT " + col + " FROM pb_players WHERE mcname = '" + name + "'");
        try {
            if (rs.next()) {
                return rs.getInt(col) == 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public ArrayList<String> getTop10() {
        this.CheckOnline();

        ArrayList<String> top = new ArrayList<String>();

        ResultSet rs = mysql.query("SELECT mcname FROM pb_players ORDER BY kills DESC LIMIT 10;");
        try {
            while (rs.next()) {
                top.add(rs.getString("mcname"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return top;

    }

}