package net.paintballcraft.plugin;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public enum RankEnum {
    ONE(new ItemStack(Material.LEATHER_CHESTPLATE), "Paintballer"),
    TWO(new ItemStack(Material.IRON_CHESTPLATE), "Fragger"),
    THREE(new ItemStack(Material.GOLD_CHESTPLATE), "Sergeant"),
    FOUR(new ItemStack(Material.DIAMOND_CHESTPLATE), "Captain"),
    FIVE(new ItemStack(Material.CHAINMAIL_CHESTPLATE), "General");

    private ItemStack chestPlate = null;
    private String stringPlate = null;

    RankEnum(ItemStack chestPlate, String stringPlate) {
        this.chestPlate = chestPlate;
        this.stringPlate = stringPlate;
    }

    public ItemStack getChest() {
        return this.chestPlate;
    }

    public String getTag() {
        return this.stringPlate;
    }

    public static RankEnum getEnum(int rank) {
        switch (rank) {
            case 1:
                return RankEnum.ONE;
            case 2:
                return RankEnum.TWO;
            case 3:
                return RankEnum.THREE;
            case 4:
                return RankEnum.FOUR;
            case 5:
                return RankEnum.FIVE;

        }
        return RankEnum.ONE;
    }



}
