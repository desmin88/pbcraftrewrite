package net.paintballcraft.plugin.workqueues;

import net.paintballcraft.plugin.database.PlayerDBBuffer;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class ReadWorker extends Thread{

    public static final Object NO_MORE_WORK = new Object();

    ReadQueue q;

    public ReadWorker(ReadQueue q) {
        this.q = q;
    }
    public void run() {
        try {
            while (true) {
                // Retrieve some work; block if the queue is empty
                Object x = q.getWork();

                // Terminate if the end-of-stream marker was retrieved
                if (x == NO_MORE_WORK) {
                    break;
                }

                // Compute the square of x
                PlayerDBBuffer dbf = (PlayerDBBuffer) x;
                dbf.read();
            }
        } catch (InterruptedException e) {
        }
    }


}
