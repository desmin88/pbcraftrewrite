package net.paintballcraft.plugin.workqueues;

import java.util.LinkedList;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class ReadQueue {

    LinkedList queue = new LinkedList();

    // ---Add work to the work queue---
    public synchronized void addWork(Object o) {
        queue.addLast(o);
        notify();
    }

    // ---Retrieve work from the work queue; block if the queue is empty---
    public synchronized Object getWork() throws InterruptedException {
        while (queue.isEmpty()) {
            wait();
        }
        return queue.removeFirst();
    }

}
