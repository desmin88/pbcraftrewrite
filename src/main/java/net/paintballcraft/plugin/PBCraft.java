package net.paintballcraft.plugin;


import com.sun.javaws.util.GeneralUtil;
import net.paintballcraft.plugin.database.DatabaseLogger;
import net.paintballcraft.plugin.database.PlayerDBBuffer;
import net.paintballcraft.plugin.json.Configuration;
import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.timers.GameStartWarning;
import net.paintballcraft.plugin.timers.InBetweenGames;
import net.paintballcraft.plugin.timers.JSONPersistence;
import net.paintballcraft.plugin.utils.GeneralUtils;
import net.paintballcraft.plugin.workqueues.ReadQueue;
import net.paintballcraft.plugin.workqueues.ReadWorker;
import net.paintballcraft.plugin.workqueues.WriteQueue;
import net.paintballcraft.plugin.workqueues.WriteWorker;
import org.bukkit.plugin.java.JavaPlugin;
import org.getspout.spout.packet.listener.PacketListeners;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class PBCraft extends JavaPlugin {

    /*
     * Just some variables needed for plugin operation
     */
    private static PBCraft pbcraft = null;

    private static DatabaseLogger db = null;

    public static final Map<String, PlayerDBBuffer> bufferMap = new HashMap<String, PlayerDBBuffer>();

    private final PBCommand pbCommand = new PBCommand();

    private static GameInstance gameInstance = null;

    private final PBListener pbListener = new PBListener();

    private final PacketModification packetModification = new PacketModification();

    private final List<String> onlineRed = new ArrayList<String>();

    private final List<String> onlineBlue = new ArrayList<String>();

    /*
     * All objects needed for the timers - futures allow us to cancel the task and get time.
     */
    public GameStartWarning gameStartWarning = new GameStartWarning();
    public InBetweenGames inBetweenGames = new InBetweenGames();

    public ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(4);
    public ScheduledFuture<?> gameClassFuture; //GameClass
    public ScheduledFuture<?> inBetweenGamesFuture; //Timer to start games
    public ScheduledFuture<?> startWarningFuture;  //Timer to show the warning that a game is starting soon
    public ScheduledFuture<?> persistenceFuture;   //Timer that periodically to our json files

    /*
     *  Read and Write queues, Read and Write threads
     */
    private static ReadQueue readQueue = new ReadQueue();
    private static WriteQueue writeQueue = new WriteQueue();
    private ReadWorker readWorker = new ReadWorker(readQueue);
    private WriteWorker writeWorker = new WriteWorker(writeQueue);

    @Override
    public void onDisable() {
      // --Server shut down, save our files---
      JSONUtils.saveConfiguration();
      try {
          this.persistenceFuture.cancel(false);
          this.inBetweenGamesFuture.cancel(true);
          this.gameClassFuture.cancel(true);
          this.startWarningFuture.cancel(true);
      } catch(Exception e) {}

      // --Safely terminate our threads--
      this.readQueue.addWork(ReadWorker.NO_MORE_WORK);
      this.writeQueue.addWork(WriteWorker.NO_MORE_WORK);

    }

    @Override
    public void onEnable() {
        // ---Set our static variable to the plugin---
        pbcraft = this;

        // ---Create our GameInstance's instance
        gameInstance = GameInstance.getInstance();

        // ---Register Events---
        getServer().getPluginManager().registerEvents(this.pbListener, this);
        PacketListeners.addListener(20, packetModification);


        // ---Prepare our json files---
        JSONUtils.serverStartup();
        Configuration cfg = JSONUtils.getConfiguration();

        // ---Create database object and then open it---
        db = new DatabaseLogger(cfg.getDbHostname(), cfg.getDbUsername(), cfg.getDbPassword(), cfg.getDbDataBase());
        db.Open();

        // ---Register all of our commands with a enhanced for-loop---
        for (String command : this.getDescription().getCommands().keySet()) {
            this.getCommand(command).setExecutor(this.pbCommand);
        }

        // ---Schedule our persistence timer with a delay of 1 minute between runs---
        this.persistenceFuture = scheduler.scheduleWithFixedDelay(new JSONPersistence(), 1, 1, TimeUnit.MINUTES);

        // --Start our worker threads--
        this.writeWorker.start();
        this.readWorker.start();

        GeneralUtils.scheduleTimers();
    }

    /**
     * Static method to get the plugin
     * @return PBCraft main plugin instance
     */
    public static PBCraft getPBCraft() {
        return pbcraft;
    }

    /**
     * Static method to get the database
     * @return DatabaseLogger instance
     */
    public static DatabaseLogger getDB() {
        return db;
    }


    public static Map<String, PlayerDBBuffer> getBuffer() {
        return bufferMap;
    }

    public static ReadQueue getReadQueue() {
        return readQueue;
    }

    public static WriteQueue getWriteQueue() {
        return writeQueue;
    }

    public List<String> getOnlineRed() {
        return onlineRed;
    }

    public List<String> getOnlineBlue() {
        return onlineBlue;
    }

}
