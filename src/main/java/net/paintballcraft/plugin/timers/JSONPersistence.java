package net.paintballcraft.plugin.timers;

import net.paintballcraft.plugin.json.JSONUtils;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class JSONPersistence implements Runnable {

    /*
     * Persists our JSON files every 1 minute
     */
    @Override
    public void run() {
        JSONUtils.saveConfiguration();
    }

}
