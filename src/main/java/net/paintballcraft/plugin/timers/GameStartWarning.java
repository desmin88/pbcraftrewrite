package net.paintballcraft.plugin.timers;

import net.paintballcraft.plugin.utils.ChatUtils;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class GameStartWarning implements Runnable {
    /*
     * Method to send messages - Sends message + true to denote we are only sending to players who have not hidden messaging
     */
    @Override
    public void run() {
        ChatUtils.globalSend("A new game is starting in 20 seconds, vote for the next map!", true);
    }
}
