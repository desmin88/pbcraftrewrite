package net.paintballcraft.plugin.timers;

import net.paintballcraft.plugin.GameInstance;
import net.paintballcraft.plugin.PBCraft;
import net.paintballcraft.plugin.commands.GeneralCommands;
import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.json.MapDefinition;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class InBetweenGames implements Runnable {

    /*
     * Called at the beginning of a new round, scheduled at the end - Starts a new round
     */
    @Override
    public void run() {
        String winningMap = "";

        List<Integer> combinedVotes = new ArrayList<Integer>();

        // --Get the combined votes of maps and add it to the list
        for (MapDefinition m : JSONUtils.getMaps().values()) {
            combinedVotes.add(m.getVotes());
        }

        // --Sort our list and then get the highest value from it--
        Collections.sort(combinedVotes);
        int highestVote = combinedVotes.get(combinedVotes.size() - 1);

        for (String s : JSONUtils.getMaps().keySet()) {
            if (JSONUtils.getMaps().get(s).getVotes() == highestVote) {
                winningMap = s;
            }
        }
        // --Clear all votes--
        for (String s : JSONUtils.getMaps().keySet()) {
            JSONUtils.getMaps().get(s).setVotes(0);
        }
        GeneralCommands.hasVoted.clear();

        // --Start the game itself--
        PBCraft.getPBCraft().gameClassFuture = PBCraft.getPBCraft().scheduler.schedule(GameInstance.getInstance(), 3, TimeUnit.MINUTES);
        GameInstance.getInstance().start(getTeamList(PBCraft.getPBCraft().getOnlineRed()), getTeamList(PBCraft.getPBCraft().getOnlineBlue()), winningMap);
    }

    /*
     * Method taking a string list of players and returns a player list
     */
    private List<Player> getTeamList(List<String> stringPlayerList) {
        List<Player> teamPlayerList = new ArrayList<Player>();
        // --Iterate through all online players, if their name is in the received string list, add them to the player list--
        for(Player p : Bukkit.getOnlinePlayers()) {
            if(stringPlayerList.contains(p.getName())) {
                teamPlayerList.add(p);
            }
        }
        return teamPlayerList;
    }



}
