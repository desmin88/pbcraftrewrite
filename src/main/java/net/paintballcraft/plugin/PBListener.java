package net.paintballcraft.plugin;

import net.paintballcraft.plugin.database.PlayerDBBuffer;
import net.paintballcraft.plugin.utils.ChatUtils;
import net.paintballcraft.plugin.utils.GeneralUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class PBListener implements Listener {

     /*
      * Handles player throwing egg grenades
      */
     @EventHandler(priority = EventPriority.LOW)
     public void onEggThrow(ProjectileHitEvent event) {
         // --Sees if the thrown entity is an egg grenade--
         if (event.getEntity() instanceof Egg) {
             Egg egg = (Egg) event.getEntity();
             Player p = (Player) egg.getShooter();
             // --Is the shooter of the egg playing in a game?--
             if (GameInstance.getInstance().isPlaying(p)) {
                 // --Create egg hit location with power of 0 for the effect only--
                 Location eggHitLocation = egg.getLocation();
                 eggHitLocation.getWorld().createExplosion(eggHitLocation, 0);
                 // --Get a list of players who were killed in the explosion--
                 List<Player> killed = new ArrayList<Player>();
                 for (Entity e : egg.getNearbyEntities(4, 4, 4)) {
                     if (e instanceof Player) {
                         killed.add( ((Player) e) );
                     }
                 }
                 for (Player killedPlayer : killed) {
                     if (GameInstance.getInstance().onSameTeam(killedPlayer, p))
                         continue;
                    GameInstance.getInstance().onHit(killedPlayer, p, RemovalCause.BLOWNUP);
                 }
             }
         }
     }

    /*
     * Handles snowball hits, void falls and general damage
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerHurt(EntityDamageEvent event) {
        // --Player fell into void--
        if(event.getCause() == EntityDamageEvent.DamageCause.VOID && event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();
            if(GameInstance.getInstance().isPlaying(p)) {
                GameInstance.getInstance().removePlayer(p, RemovalCause.VOID);
                event.setCancelled(true);
                return;
            }
        }
        // --Player was hurt by another entity, generally a snowball-
        if (event instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damagedByEntity = (EntityDamageByEntityEvent) event;

            if (event.getEntity() instanceof Player && damagedByEntity.getDamager() instanceof Snowball) {

                Player hit = (Player) event.getEntity();
                Snowball sb = (Snowball) damagedByEntity.getDamager();
                Player shooter = (Player) sb.getShooter();

                if (hit.getEyeLocation().distance(sb.getLocation()) < .5) {
                    GameInstance.getInstance().onHit(hit, shooter, RemovalCause.HEADSHOT);
                    return;
                }
                if (hit.getLocation().distance(shooter.getLocation()) > 25) {
                    GameInstance.getInstance().onHit(hit, shooter, RemovalCause.FAR);
                    return;
                }

                GameInstance.getInstance().onHit(hit, shooter, RemovalCause.NORMAL);
                event.setCancelled(true);
            }
        }
        if(event.getEntity() instanceof Player) {
            event.setCancelled(true);
        }
    }


    /*
     * Handles a player talking in chat
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerChat(PlayerChatEvent event) {
        ChatColor color;
        Player p = event.getPlayer();
        if (!PBCraft.getPBCraft().getOnlineBlue().contains(p.getName()) && !PBCraft.getPBCraft().getOnlineRed().contains(p.getName())) {
            color = ChatColor.WHITE;
        } else {
            color = (PBCraft.getPBCraft().getOnlineBlue().contains(p.getName())) ? ChatColor.BLUE : ChatColor.RED;
        }
        if (PBCraft.getBuffer().get(p.getName()).isDonor()) {
            event.setFormat(ChatColor.LIGHT_PURPLE + "[General+]" + ChatColor.GREEN + "[Donor]" + color + event.getPlayer().getName() + ": " + ChatColor.WHITE + event.getMessage());
        } else {
            event.setFormat(ChatColor.LIGHT_PURPLE + "[" + GeneralUtils.getStringPlate(PBCraft.getBuffer().get(p.getName()).getRank()) + "]" + color + p.getName() + ": " + ChatColor.WHITE + event.getMessage());
        }

    }


    /*
     * Handles a player joining the game
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        // --They are not in the buffer, make them one
        if(PBCraft.getBuffer().get(p.getName()) == null) {
            PBCraft.getBuffer().put(p.getName(), new PlayerDBBuffer(p.getName()));
        } else {
            PBCraft.getReadQueue().addWork(PBCraft.getBuffer().get(p.getName()));
        }
        event.getPlayer().setFoodLevel(20);
        event.getPlayer().teleport(GeneralUtils.getLobby());
        GeneralUtils.giveItems(p);
    }


    /*
     * Handles players logging on, NOTE: This is before playerjoinevent
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerLogin(PlayerLoginEvent event) {
        Player p = event.getPlayer();
        // --Necessary, we should access the DB instead of making them a buffer object--
        boolean isDonor = PBCraft.getDB().isDonor(p.getName());
        boolean isFull = Bukkit.getOnlinePlayers().length == Bukkit.getMaxPlayers();
        // --Server is full--
        if(isFull)
        {
            // --Player is donor--
            if(isDonor) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    if(!PBCraft.getBuffer().get(player.getName()).isDonor()) {
                        p.kickPlayer("Sorry, you were kicked to make room for a donor member joining.");
                        if(GameInstance.getInstance().isPlaying(p)) {
                            GameInstance.getInstance().removePlayer(p, RemovalCause.LEFTGAME);
                        }
                    }
                }
                // --After kicking a player to make room, allow the event.
                event.allow();
            }
            if(!isDonor) {
                event.disallow(PlayerLoginEvent.Result.KICK_FULL, "The server is full. If you purchase a membership, you get reserved slots.");
            }
        }
        // --Server isn't full, allow player--
        if(!isFull)
        {
            event.allow();
        }


    }

     /*
      * Handles players leaving - Writes their data into the database and removes them from game if they're playing
      */
     @EventHandler(priority = EventPriority.LOW)
     public void onPlayerLeave(PlayerQuitEvent event) {
         Player p = event.getPlayer();
         PBCraft.getWriteQueue().addWork(PBCraft.getBuffer().get(p.getName()));

         PBCraft.getPBCraft().getOnlineBlue().remove(p.getName());
         PBCraft.getPBCraft().getOnlineRed().remove(p.getName());
         if (GameInstance.getInstance().isPlaying(p)) {
             GameInstance.getInstance().removePlayer(p, RemovalCause.LEFTGAME);
         }
     }

    /*
     * Players cannot drop snowballs out of their inventory
     */
    @EventHandler(priority = EventPriority.LOW)
    public void denyDrops(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    /*
     * Disallows removing items from inventory
     */
    @EventHandler(priority = EventPriority.LOW)
    public void denyClick(InventoryClickEvent event) {
        event.setCancelled(true);
    }

    /*
     * Makes players always have full health
     */
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerHunger(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

}
