package net.paintballcraft.plugin;

import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.utils.ChatUtils;
import net.paintballcraft.plugin.utils.GeneralUtils;
import net.paintballcraft.plugin.utils.PacketUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class GameInstance implements Runnable{

     /**
     * Methods and fields to make this class a Singleton
     */
    private GameInstance() {}

    private static GameInstance instance = null;

    public static GameInstance getInstance() {
        if (instance == null) {
            instance = new GameInstance();
        }
        return instance;
    }

    /*
     * Information on the current running game
     */
    public boolean isRunning = false;

    private List<Player> overallRed = new ArrayList<Player>();
    private List<Player> overallBlue = new ArrayList<Player>();

    private List<Player> red = new ArrayList<Player>();
    private List<Player> blue = new ArrayList<Player>();

    private Location redspawn;
    private Location bluespawn;

    /*
     * Utility objects
     */
    private Random r = new Random();
    private ItemStack redWool = new ItemStack(Material.WOOL, 1, (short) 14);
    private ItemStack blueWool = new ItemStack(Material.WOOL, 1, (short) 11);

    /*
     * Used to remove a player from the match for whatever reason specified
     */
    public void removePlayer(Player p, RemovalCause rc) {
        ChatUtils.globalSend(p.getName() + rc.getText(), true); //
        red.remove(p);
        blue.remove(p);
        p.getInventory().setHelmet(null);
        PacketUtils.undisguise(p);
        p.teleport(GeneralUtils.getLobby());
        if (blue.isEmpty()) {
            this.end( (Player) red.toArray()[r.nextInt(red.size())]);
        }
        else if (red.isEmpty()) {
            this.end( (Player) blue.toArray()[r.nextInt(blue.size())]);
        }
        PBCraft.getWriteQueue().addWork(PBCraft.getBuffer().get(p.getName()));
    }

    /*
     * Called to start a game
     */
    public synchronized void start(List<Player> redP, List<Player> blueP, String map) {
        if (blueP.isEmpty() || redP.isEmpty()) {
            ChatUtils.globalSend("No game starting due to lack of players", true); //
            GeneralUtils.cleanupTasks();
            GeneralUtils.scheduleTimers();
            return;
        }

        red.addAll(redP);
        blue.addAll(blueP);
        overallRed.addAll(redP);
        overallBlue.addAll(blueP);
        System.out.println("------------------");
        System.out.println("Beginning of game");
        System.out.println("overallBlue: " + this.overallBlue.toString());
        System.out.println("overallRed: " + this.overallRed.toString());
        System.out.println("blue: " + blue.toString());
        System.out.println("red: " + red.toString());
        System.out.println("------------------");

        this.isRunning = true;

        if (map.equals("")) { // No votes?!
            map = getRandomMap();
            ChatUtils.globalSend("Map: " + map + " was selected and a game has started!", true);
        } else {
            ChatUtils.globalSend("Map: " + map + " was selected and a game has started!", true);
        }

        this.redspawn = GeneralUtils.getRedSpawn(map);
        this.bluespawn = GeneralUtils.getBlueSpawn(map);

        for(Player player : this.red) {
            PacketUtils.disguise(player);
            GeneralUtils.giveItems(player);
            this.giveEffects(player);
            player.getInventory().setHelmet(this.redWool);
            player.teleport(redspawn);
        }
         for(Player player : this.blue) {
            PacketUtils.disguise(player);
            GeneralUtils.giveItems(player);
            this.giveEffects(player);
            player.getInventory().setHelmet(this.blueWool);
            player.teleport(bluespawn);
        }


    }

  /*
   * Called when a game ends due to a team being completely killed
   */
    public void end(Player winningKiller) {
        // --No more things relating to a game can happen, we're closed for business--
        this.isRunning = false;
        String winningTeam = "";
        if(winningKiller == null) {
            ChatUtils.globalSend("The game was a draw! The next match starts in 60 seconds! Don't forget to vote!", true);
        } else {
            // --Nifty ternary to get our winning team--
            winningTeam = ((this.blue.contains(winningKiller)) ? "blue" : "red");
            PBCraft.getBuffer().get(winningKiller.getName()).addPoints(JSONUtils.getConfiguration().getPointsForWinningKill());
            ChatUtils.globalSend("Player " + winningKiller.getName() + " wins the round for " + winningTeam + " team!", true);
            ChatUtils.globalSend("Each winning player received " + JSONUtils.getConfiguration().getPointsGivenToWinningTeam() + " points!", true);
            ChatUtils.globalSend("Get ready! The next match starts in 60 seconds! Don't forget to vote!", false);
        }

        // --Check to see if anybody should get a new rank --
        GeneralUtils.checkRanks();

        for(Player player : this.overallRed) {
            player.getInventory().setHelmet(null);
            PacketUtils.undisguise(player);
            player.teleport(GeneralUtils.getLobby());
            PBCraft.getWriteQueue().addWork(PBCraft.getBuffer().get(player.getName()));
        }
        for(Player player : this.overallBlue) {
            player.getInventory().setHelmet(null);
            PacketUtils.undisguise(player);
            player.teleport(GeneralUtils.getLobby());
            PBCraft.getWriteQueue().addWork(PBCraft.getBuffer().get(player.getName()));
        }

        // --Award the winning team their points
        if(winningTeam.equals("blue"))
            for(Player player : overallBlue)
                PBCraft.getBuffer().get(player.getName()).addPoints(JSONUtils.getConfiguration().getPointsGivenToWinningTeam());
        else if(winningTeam.equals("red"))
            for(Player player : overallRed)
                PBCraft.getBuffer().get(player.getName()).addPoints(JSONUtils.getConfiguration().getPointsGivenToWinningTeam());

        // --Empty out our lists--
        red.clear();
        blue.clear();
        overallBlue.clear();;
        overallRed.clear();

        // --Cleanup old tasks and schedule new ones--
        GeneralUtils.cleanupTasks();
        GeneralUtils.scheduleTimers();
    }

    /*
     * Called when the GameInstance timer runs down and a game ends as a draw
     */
    @Override
    public void run() {
        this.end(null);
    }

    /*
     * Called when a player is hit with a snowball
     */
    public void onHit(Player hit, Player shooter, RemovalCause rc) {
        // --Checks to see if we should ignore this shot--
        if (!isPlaying(hit))
            return;
        if (!isPlaying(shooter))
            return;
        if (onSameTeam(hit, shooter))
            return;

        // --Teleport the shot player to lobby and give them some balls
        hit.teleport(GeneralUtils.getLobby());
        GeneralUtils.giveItems(hit);

        // --Award points to shooter for a hit and update their frag amount
        PBCraft.getBuffer().get(shooter.getName()).addPoints(JSONUtils.getConfiguration().getPointsPerKill());
        PBCraft.getBuffer().get(shooter.getName()).addKills(1);
        ChatUtils.send( "Nice shot! +" + JSONUtils.getConfiguration().getPointsPerKill() + " points.", shooter);

        // --Give the dead player their onDeath points and remove them from the lists - NOTE: We do not remove them from the overall lists
        PBCraft.getBuffer().get(hit.getName()).addPoints(JSONUtils.getConfiguration().getPointsGivenOnDeath());
        this.red.remove(hit);
        this.blue.remove(hit);

        ChatColor hitColor = getTeamColor(hit);
        ChatColor shooterColor = getTeamColor(shooter);

        if (red.isEmpty() || blue.isEmpty()) { // --Game winning kill--
            ChatUtils.globalSend(hitColor + hit.getName() + ChatColor.GOLD + rc.getText() + shooterColor + shooter.getName() + "!", true);
            end(shooter);
        } else { // --Regular death--
            ChatUtils.globalSend(hitColor + hit.getName() + ChatColor.GOLD + rc.getText() + shooterColor + shooter.getName() + "!", true);
        }
    }

    public String getRandomMap() {
        r.setSeed( (System.currentTimeMillis() + System.nanoTime()) / System.currentTimeMillis());
        return (String) JSONUtils.getMaps().keySet().toArray()[r.nextInt(JSONUtils.getMaps().keySet().size())];
    }

    public void giveEffects(Player p) {
        switch (PBCraft.getBuffer().get(p.getName()).getSpeed()) {
            case 1:
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1));
                PBCraft.getBuffer().get(p.getName()).setSpeed(0);
                break;
            case 3:
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3600, 1));
                PBCraft.getBuffer().get(p.getName()).setSpeed(0);
                break;
            case 5:
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 5000, 1));
                PBCraft.getBuffer().get(p.getName()).setSpeed(0);
                break;
        }
        switch (PBCraft.getBuffer().get(p.getName()).getJump()) {
            case 1:
                p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1200, 1));
                PBCraft.getBuffer().get(p.getName()).setJump(0);
                break;
            case 3:
                p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 3600, 1));
                PBCraft.getBuffer().get(p.getName()).setJump(0);
                break;
            case 5:
                p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 5000, 1));
                PBCraft.getBuffer().get(p.getName()).setJump(0);
                break;
        }
    }

    /*
     * Convenience methods
     */

    public boolean onSameTeam(Player p, Player p2) {
        return blue.contains(p) && blue.contains(p2) || red.contains(p2) && red.contains(p);
    }


    public boolean isPlaying(String p) {
        String player = ChatColor.stripColor(p);
        System.out.println("------------------");
        System.out.println("isPlaying:" + p);
        System.out.println("isRunning" + this.isRunning);
        System.out.println("red contains bukkit.get player" + this.red.contains(Bukkit.getPlayer(p)));
        System.out.println("blue contains bukkit.get player" + this.blue.contains(Bukkit.getPlayer(p)));
        System.out.println("Value returned:" + (this.isRunning && (this.red.contains(Bukkit.getPlayer(p)) || this.blue.contains(Bukkit.getPlayer(p)))));
        System.out.println("overallBlue: " + this.overallBlue.toString());
        System.out.println("overallRed: " + this.overallRed.toString());
        System.out.println("blue: " + blue.toString());
        System.out.println("red: " + red.toString());
        System.out.println("------------------");
        return this.isRunning && (this.red.contains(Bukkit.getPlayer(player)) || this.blue.contains(Bukkit.getPlayer(player)));
    }

    public boolean isPlaying(Player p) {
        return this.isRunning && (this.red.contains(p) || this.blue.contains(p));
    }

    public ChatColor getTeamColor(String p) {
        if(PBCraft.getPBCraft().getOnlineBlue().contains(p)) {
            return ChatColor.BLUE;
        } else {
            return ChatColor.RED;
        }
    }

    public ChatColor getTeamColor(Player p) {
        if(PBCraft.getPBCraft().getOnlineBlue().contains(p.getName())) {
            return ChatColor.BLUE;
        } else {
            return ChatColor.RED;
        }
    }



}
