package net.paintballcraft.plugin;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public enum RemovalCause {

    HEADSHOT(" was shot in the face by "), NORMAL(" was fragged by "), BLOWNUP(" was blown up by "), FAR(" was shot from afar by "), VOID(" fell into the void!"), LEFTGAME(" left the game!");


    private final String removeText;

    private RemovalCause(String s) {
        this.removeText = s;
    }

    public String getText() {
        return this.removeText;
    }

}
