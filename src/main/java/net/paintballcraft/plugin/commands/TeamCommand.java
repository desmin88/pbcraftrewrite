package net.paintballcraft.plugin.commands;

import net.paintballcraft.plugin.PBCraft;
import net.paintballcraft.plugin.utils.ChatUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class TeamCommand {

    /*
     * Handles /red, /blue, and /leave
     */
    public static boolean onCommand(CommandSender sender, Command command, String c, String[] args) {
        // --A console is sending these commands, deny them--
        if(!(sender instanceof Player)) {
             sender.sendMessage("These commands cannot be executed from the console");
             return true;
         }
        Player player = (Player) sender;

        // --Joining red team--
        if(c.equalsIgnoreCase("red")) {
            joinTeam(player, "red");
            return true;
        }

        // --Joining blue team--
        if(c.equalsIgnoreCase("blue")) {
            joinTeam(player, "blue");
            return true;
        }

        // --Leaving their team--
        if(c.equalsIgnoreCase("leave")) {
            PBCraft.getPBCraft().getOnlineBlue().remove(player.getName());
            PBCraft.getPBCraft().getOnlineRed().remove(player.getName());
            player.setPlayerListName(ChatColor.stripColor(player.getName()));
            ChatUtils.send("You have left your team!", player);
        }
        return true;
    }

    /*
     * Method which adds a player to a certain team
     */
    public static void joinTeam(Player player, String teamToJoin) {
        // --Joining red team--
        if(teamToJoin.equals("red")) {

            // --Already in red team--
            if (PBCraft.getPBCraft().getOnlineRed().contains(player.getName())) {
                ChatUtils.send("You are already in red team!", player);
                return;
            }

            // --Joining red would induce an unbalancing of teams
            if(PBCraft.getPBCraft().getOnlineRed().size() - PBCraft.getPBCraft().getOnlineBlue().size() > 2) {
                ChatUtils.send("Red team is full! Please join blue!", player);
                return;
            }

            // --Allowed to join red team now--
            PBCraft.getPBCraft().getOnlineBlue().remove(player.getName());
            PBCraft.getPBCraft().getOnlineRed().add(player.getName());
            // --Submit their name to the playerlist
            String name = ChatColor.RED + player.getName();
            if(name.length() > 16)
                name = name.substring(0, 16);
            player.setPlayerListName(name);

            ChatUtils.send("You have joined red team!", player);
            return;
        }
        // --Joining blue team--
        else if(teamToJoin.equals("blue")) {
            // --Already in blue team--
            if (PBCraft.getPBCraft().getOnlineBlue().contains(player.getName())) {
                ChatUtils.send("You are already in blue team!", player);
                return;
            }

            // --Joining blue would induce an unbalancing of teams
            if(PBCraft.getPBCraft().getOnlineBlue().size() - PBCraft.getPBCraft().getOnlineRed().size() > 2) {
                ChatUtils.send("Blue team is full! Please join red!", player);
                return;
            }

            // --Allowed to join blue team now--
            PBCraft.getPBCraft().getOnlineRed().remove(player.getName());
            PBCraft.getPBCraft().getOnlineBlue().add(player.getName());

            // --Submit their name to the playerlist
            String name = ChatColor.BLUE + player.getName();
            if(name.length() > 16)
                name = name.substring(0, 16);
            player.setPlayerListName(name);

            ChatUtils.send("You have joined blue team!", player);
            return;
        }
    }
}
