package net.paintballcraft.plugin.commands;

import net.paintballcraft.plugin.GameInstance;
import net.paintballcraft.plugin.PBCraft;
import net.paintballcraft.plugin.database.PlayerDBBuffer;
import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.utils.ChatUtils;
import net.paintballcraft.plugin.utils.GeneralUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class BuyCommand {

    /*
     * Handles solely the buy command
     */
    public static boolean onCommand(CommandSender sender, Command command, String c, String[] args) {
        Player player = (Player) sender;
        PlayerDBBuffer playerBuffer = PBCraft.getBuffer().get(player.getName());
        ChatUtils.send(ChatColor.RED + "Donate and all items are 50% off!", player);

        // --If no arguments are typed, show some info--
        if (args.length == 0) {
            ChatUtils.send("Type /buy snowballs [amount] to buy in stacks of 16.", player);
            ChatUtils.send("Type /buy grenades [amount] to buy in amounts of 1", player);
            ChatUtils.send("Type /buy speed [1,3,5] to buy a speed potion lasting 1, 3 or 5 minutes.", player);
            ChatUtils.send("Type /buy jump [1,3,5] to buy a jump potion lasting 1, 3 or 5 minutes.", player);
            ChatUtils.send("To check an items price, just type /buy [item]", player);
            return true;
        }

        // --Purchasing grenades--
        if (args[0].equalsIgnoreCase("grenades")) {
            // --Price checking grenades, no quantity typed--
            if (args.length == 1) {
                ChatUtils.send("Current price of eggs is " + JSONUtils.getConfiguration().getPricePerEgg() + " for one.", player);
                return true;
            }

            // --Getting price of the items they want, checking against their own points--
            int priceToPay = 0;
            int playerPoints = playerBuffer.getPoints();
            try {
                priceToPay = Integer.parseInt(args[1]) * JSONUtils.getConfiguration().getPricePerEgg();
            } catch (Exception e) {
                ChatUtils.send("You must enter the number of eggs you want to purchase.", player);
                return true;
            }
            // --Cut price in half for donors--
            if (playerBuffer.isDonor())
                priceToPay = priceToPay / 2;
            if (priceToPay > playerPoints) {
                ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerPoints + " points.", player);
                return true;
            }
            // --A game is not running, so we have set their eggs in the buffer--
            if (!GameInstance.getInstance().isRunning) {
                playerBuffer.setEggs(Integer.parseInt(args[1]) + playerBuffer.getEggs());
                playerBuffer.setPoints( playerBuffer.getPoints() - priceToPay);
                ChatUtils.send("Successfully bought your items and you will receive them the next game.", player);
                return true;
            }
            // --They're in a game, so lets add to inventory now
            if (GameInstance.getInstance().isRunning && GameInstance.getInstance().isPlaying(player)) {
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                for (int i = 0; i < Integer.parseInt(args[1]); i++) {
                    player.getInventory().addItem(GeneralUtils.egg);
                }
                ChatUtils.send("Enjoy your items.", player);
                return true;
            }

        }

        // --Purchasing snowballs-
        if (args[0].equalsIgnoreCase("snowballs")) {
            // --Just a pricecheck--
            if (args.length == 1) {
                ChatUtils.send("Current price of snowballs is " + JSONUtils.getConfiguration().getPriceForSnowBallStack() + " points per stack of 16.", player);
                return true;
            }
            // --Calculating price to pay--
            int priceToPay = 0;
            int playerPoints = playerBuffer.getPoints();
            try {
                priceToPay = Integer.parseInt(args[1]) * JSONUtils.getConfiguration().getPriceForSnowBallStack();
            } catch (Exception e) {
                ChatUtils.send("You must enter the number of stacks you want to purchase.", player);
                return true;
            }

            // --Cut price in half for donor status--
            if (playerBuffer.isDonor())
                priceToPay = priceToPay / 2;
            if (priceToPay > playerPoints) {
                ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerPoints + " points.", player);
                return true;
            }

            // --Game is not running, add the purchased snowballs to the buffer--
            if (!GameInstance.getInstance().isRunning) {
                playerBuffer.setSnowballs(Integer.parseInt(args[1]) + playerBuffer.getSnowballs());
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                ChatUtils.send("Successfully bought your items and you will receive them the next game.", player);
                return true;
            }

            // --Game is running, add snowballs to inventory
            if (GameInstance.getInstance().isRunning && GameInstance.getInstance().isPlaying(player)) {
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                for (int i = 0; i < Integer.parseInt(args[1]); i++) {
                    player.getInventory().addItem(GeneralUtils.snowBalls);
                }
                ChatUtils.send("Enjoy your items.", player);
                return true;
            }

        }

        // --Purchasing a speed potion--
        if (args[0].equalsIgnoreCase("speed")) {

            if (args.length == 1) {
                ChatUtils.send("Current price of a speed potion is " + JSONUtils.getConfiguration().getPriceForSpeed1M() + "," + JSONUtils.getConfiguration().getPriceForSpeed3M() + "," + JSONUtils.getConfiguration().getPriceForSpeed5M() + " for 1, 3, and 5 minutes.", player);
                return true;
            }
            if (Integer.parseInt(args[1]) == 1) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForSpeed1M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1));
                    ChatUtils.send("You bought one minute of speed.", player);
                    return true;
                } else {
                    playerBuffer.setSpeed(1);
                    ChatUtils.send("Next game you will receive your speed potion.", player);
                    return true;
                }

            }
            if (Integer.parseInt(args[1]) == 3) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForSpeed3M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 3600, 1));
                    ChatUtils.send("You bought 3 minutes of speed.", player);
                    return true;
                } else {
                    playerBuffer.setSpeed(3);
                    ChatUtils.send("Next game you will receive your speed potion.", player);
                    return true;
                }
            }

            if (Integer.parseInt(args[1]) == 5) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForSpeed5M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 6000, 1));
                    ChatUtils.send("You bought 5 minutes of speed.", player);
                    return true;
                } else {
                    playerBuffer.setSpeed(5);
                    ChatUtils.send("Next game you will receive your speed potion.", player);
                    return true;
                }
            }

        }
        if (args[0].equalsIgnoreCase("jump")) {
            if (args.length == 1) {
                ChatUtils.send("Current price of a jumping potion is " + JSONUtils.getConfiguration().getPriceForJump1M() + "," + JSONUtils.getConfiguration().getPriceForJump3M() + "," + JSONUtils.getConfiguration().getPriceForJump5M() + " for 1, 3, and 5 minutes.", player);
                return true;
            }
            if (Integer.parseInt(args[1]) == 1) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForJump1M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 1200, 1));
                    ChatUtils.send("You bought one minute of jumping.", player);
                    return true;
                } else {
                    playerBuffer.setJump(1);
                    ChatUtils.send("Next game you will receive your jumping potion.", player);
                    return true;
                }

            }
            if (Integer.parseInt(args[1]) == 3) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForJump3M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 3600, 1));
                    ChatUtils.send("You bought 3 minutes of jumping.", player);
                    return true;
                } else {
                    playerBuffer.setJump(3);
                    ChatUtils.send("Next game you will receive your jumping potion.", player);
                    return true;
                }
            }

            if (Integer.parseInt(args[1]) == 5) {
                int priceToPay = JSONUtils.getConfiguration().getPriceForJump5M();
                if (playerBuffer.isDonor())
                    priceToPay = priceToPay / 2;
                if (playerBuffer.getPoints() < priceToPay) {
                    ChatUtils.send("You don't have enough points to buy this item! You currently have " + playerBuffer.getPoints() + " points.", player);
                    return true;
                }
                playerBuffer.setPoints(playerBuffer.getPoints() - priceToPay);
                if (GameInstance.getInstance().isRunning) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 6000, 1));
                    ChatUtils.send("You bought 5 minutes of jumping.", player);
                    return true;
                } else {
                    playerBuffer.setJump(5);
                    ChatUtils.send("Next game you will receive your jumping potion.", player);
                    return true;
                }
            }

        }
        return false;
    }
}
