package net.paintballcraft.plugin.commands;

import net.paintballcraft.plugin.GameInstance;
import net.paintballcraft.plugin.PBCraft;
import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.json.MapDefinition;
import net.paintballcraft.plugin.utils.ChatUtils;
import net.paintballcraft.plugin.utils.GeneralUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class GeneralCommands {

    // --List of people who have voted--
    public static List<String> hasVoted = new ArrayList<String>();

    /*
     * Handles togglemsg, time, points, leaderboard
     */
    public static boolean onCommand(CommandSender sender, Command command, String c, String[] args) {
        Player player = (Player) sender;

        // --Toggles hiding messages
        if(c.equals("togglemsg")) {
            if (PBCraft.getBuffer().get(player.getName()).isHide()) {
                PBCraft.getBuffer().get(player.getName()).setHide(false);
                ChatUtils.send("You are seeing messages.", player);
                return true;
            }
            if (!PBCraft.getBuffer().get(player.getName()).isHide()) {
                PBCraft.getBuffer().get(player.getName()).setHide(true);
                ChatUtils.send("You have hidden messages.", player);
                return true;
            }
        }

        // --Player wants to know time remaining
        if(c.equals("time")) {
            if (!GameInstance.getInstance().isRunning) {
                ChatUtils.send("No game is currently playing", player);
                return true;
            } else {
                ChatUtils.send("There are " + PBCraft.getPBCraft().gameClassFuture.getDelay(TimeUnit.SECONDS) + " second(s) remaining.", player);
                return true;
            }
        }

        // --Player wants to know how many points they have --
        if(c.equals("points")) {
            ChatUtils.send("Your points: " + PBCraft.getBuffer().get(player.getName()).getPoints(), player);
            return true;
        }

        // --Wants to view leaderboard--
        if(c.equals("leaderboard")) {
            if (c.equalsIgnoreCase("leaderboard")) {
                GeneralUtils.sendLeaderBoard(player);
                return true;
            }
        }

        /*
         * Handles player voting
         */
        if(c.equals("vote")) {
            if (args.length == 0) {
                ChatUtils.send("Type /vote list to see a list of maps, then type /vote [map] to vote for it.", player);
                return true;
            }
            if (args[0].equalsIgnoreCase("list")) {
                List<String> temp = new ArrayList<String>();
                temp.addAll(JSONUtils.getMaps().keySet());
                temp.remove("lobby");
                ChatUtils.send(temp.toString(), player);
                return true;
            }
            if (hasVoted.contains(player.getName())) {
                ChatUtils.send("You have already voted this round!", player);
                return true;
            }
            if (GameInstance.getInstance().isRunning) {
                ChatUtils.send("A game is in session, please vote after.", player);
                return true;
            }
            if (!JSONUtils.getMaps().keySet().contains(args[0].toLowerCase())) {
                ChatUtils.send("That is not a valid map, please type /vote list to see all available maps.", player);
                return true;
            }
            // valid vote now
            hasVoted.add(player.getName()); // Add them to the votes
            MapDefinition chosenMap = JSONUtils.getMaps().get(args[0].toLowerCase());
            chosenMap.setVotes(chosenMap.getVotes() + 1);
            ChatUtils.send("Your vote has been cast!", player);
            return true;
        }
        return true;
    }


}
