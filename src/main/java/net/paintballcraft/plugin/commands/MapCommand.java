package net.paintballcraft.plugin.commands;

import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.json.MapDefinition;
import net.paintballcraft.plugin.utils.ChatUtils;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class MapCommand{

    /*
     * Handles /pb
     */
    public static boolean onCommand(CommandSender sender, Command command, String c, String[] args) {
        Player player = (Player) sender;
        Location location = player.getLocation();
        // --Assume there setting the lobby here.
        if(c.equals("pb") && player.isOp() && args.length == 2) {
            if(args[0].equals("set") && args[1].equals("lobby")) {
                MapDefinition mapDefinition = JSONUtils.getMaps().get("lobby");
                mapDefinition.setWorld(location.getWorld().getName());
                mapDefinition.setBlueX(location.getBlockX());
                mapDefinition.setBlueY(location.getBlockY());
                mapDefinition.setBlueZ(location.getBlockZ());
                ChatUtils.send("You succesfully set the spawn for the lobby.", player);
                JSONUtils.saveConfiguration();
                return true;
            }
        }

        // --Assume their setting spawns for a map--
        if (c.equals("pb") && player.isOp() && args.length == 3) {
            if (args[2].equals("blue")) {
                    MapDefinition mapDefinition = null;
                    if (JSONUtils.getMaps().get(args[1].toLowerCase()) == null) {
                        mapDefinition = new MapDefinition();
                        mapDefinition.setWorld(location.getWorld().getName());
                        mapDefinition.setBlueX(location.getBlockX());
                        mapDefinition.setBlueY(location.getBlockY());
                        mapDefinition.setBlueZ(location.getBlockZ());
                        JSONUtils.getMaps().put(args[1].toLowerCase(), mapDefinition);
                    } else {
                        mapDefinition = JSONUtils.getMaps().get(args[1].toLowerCase());
                        mapDefinition.setWorld(location.getWorld().getName());
                        mapDefinition.setBlueX(location.getBlockX());
                        mapDefinition.setBlueY(location.getBlockY());
                        mapDefinition.setBlueZ(location.getBlockZ());
                    }

                    ChatUtils.send("Successfully set blue spawn for " + args[1].toLowerCase(), player);
                    JSONUtils.saveConfiguration();
                    return true;
                }
                if (args[2].equals("red")) {
                    MapDefinition mapDefinition = null;
                    if (JSONUtils.getMaps().get(args[1].toLowerCase()) == null) {
                        mapDefinition = new MapDefinition();
                        mapDefinition.setWorld(location.getWorld().getName());
                        mapDefinition.setRedX(location.getBlockX());
                        mapDefinition.setRedY(location.getBlockY());
                        mapDefinition.setRedZ(location.getBlockZ());
                        JSONUtils.getMaps().put(args[1].toLowerCase(), mapDefinition);
                    } else {
                        mapDefinition = JSONUtils.getMaps().get(args[1].toLowerCase());
                        mapDefinition.setWorld(location.getWorld().getName());
                        mapDefinition.setRedX(location.getBlockX());
                        mapDefinition.setRedY(location.getBlockY());
                        mapDefinition.setRedZ(location.getBlockZ());
                    }

                    ChatUtils.send("Successfully set red spawn for " + args[1].toLowerCase(), player);
                    JSONUtils.saveConfiguration();
                    return true;
                }
            }
            return true;
        }
    }


