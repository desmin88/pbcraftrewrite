package net.paintballcraft.plugin.utils;

import net.minecraft.server.Packet20NamedEntitySpawn;
import net.minecraft.server.Packet29DestroyEntity;
import net.paintballcraft.plugin.GameInstance;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.player.SpoutPlayer;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class PacketUtils {

    /*
     * Used to disguise a player
     */
    public static void disguise(Player toUnDisguise) {
        SpoutPlayer spoutToUnDisguise = (SpoutPlayer) toUnDisguise;

        // --Make spawn packet with added colors for the player--
        Packet20NamedEntitySpawn p20 = new Packet20NamedEntitySpawn(((CraftPlayer) toUnDisguise).getHandle());
        p20.b = GameInstance.getInstance().getTeamColor(p20.b) + p20.b;

        // --Make the destroy entity packet for the player to remove their old entity--
        Packet29DestroyEntity p29 = new Packet29DestroyEntity(toUnDisguise.getEntityId());

        // --Iterate through all online players--
        for (Player player : Bukkit.getOnlinePlayers()) {

            // --If the current player iteration is equal to the player were going to disguise, jump to the next iteration--
            if (player == toUnDisguise) {
                continue;
            }

            SpoutPlayer spoutPlayer = (SpoutPlayer) player;

            // --If the current player were on is spout craft enabled, treat them different
            if(spoutPlayer.isSpoutCraftEnabled()) {
                // --Set the title for the player to be disguised for the current player iteration to a colored name
                spoutToUnDisguise.setTitleFor(spoutPlayer, p20.b);
                continue;
            }
            // --The current iteration player is not spoutcraft enabled, send them the necessary packets to disguise
            ((CraftPlayer) player).getHandle().netServerHandler.sendPacket(p29);
            ((CraftPlayer) player).getHandle().netServerHandler.sendPacket(p20);
        }
    }







    /*
     * Used to undisguise a player
     */
    public static void undisguise(Player toUnDisguise) {
        SpoutPlayer spoutToUnDisguise = (SpoutPlayer) toUnDisguise;

        // --Make spawn packet with stripped colors for the player to be undisguised--
        Packet20NamedEntitySpawn p20 = new Packet20NamedEntitySpawn(((CraftPlayer) toUnDisguise).getHandle());
        p20.b = ChatColor.stripColor(p20.b);

        // --Make the destroy entity packet for the player to be undisguised--
        Packet29DestroyEntity p29 = new Packet29DestroyEntity(toUnDisguise.getEntityId());

        // --Iterate through all online players--
        for (Player player : Bukkit.getOnlinePlayers()) {

            // --If the current player iteration is equal to the player were going to un disguise, jump to the next iteration--
            if (player == toUnDisguise) {
                continue;
            }

            SpoutPlayer spoutPlayer = (SpoutPlayer) player;
            // --If the current player were on is spout craft enabled, treat them different
            if(spoutPlayer.isSpoutCraftEnabled()) {
                // --Set the title for the player to be undisguised for the current player iteration to a stripped color name
                spoutToUnDisguise.setTitleFor(spoutPlayer, ChatColor.stripColor(spoutToUnDisguise.getName()));
                continue;
            }
            // --The current iteration player is not spoutcraft enabled, send them the necessary packets to undisguise
            ((CraftPlayer) player).getHandle().netServerHandler.sendPacket(p29);
            ((CraftPlayer) player).getHandle().netServerHandler.sendPacket(p20);
        }
    }

}
