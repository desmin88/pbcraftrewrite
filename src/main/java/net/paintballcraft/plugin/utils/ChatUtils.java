package net.paintballcraft.plugin.utils;

import net.paintballcraft.plugin.PBCraft;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class ChatUtils {

    /**
     * Sends a message to all players - whether or not they disabled messages
     * @param message sent to players globally
     */
    public static void globalSend(String message) {
       globalSend(message, false);
    }

    public static void globalSend(String message, boolean bool) {
    for(Player player : Bukkit.getOnlinePlayers())     {
            if(!bool) {
                player.sendMessage(ChatColor.DARK_AQUA + "[Paintball] " + ChatColor.GOLD + message);
            }
            else {
                if(!PBCraft.getBuffer().get(player.getName()).isHide()) {
                    player.sendMessage(ChatColor.DARK_AQUA + "[Paintball] " + ChatColor.GOLD + message);
                }

            }

        }
    }

    public static void send(String message, Player player) {
        player.sendMessage(ChatColor.DARK_AQUA + "[Paintball] " + ChatColor.GOLD + message);
    }
}



