package net.paintballcraft.plugin.utils;

import net.paintballcraft.plugin.PBCraft;
import net.paintballcraft.plugin.RankEnum;
import net.paintballcraft.plugin.database.PlayerDBBuffer;
import net.paintballcraft.plugin.json.JSONUtils;
import net.paintballcraft.plugin.json.MapDefinition;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import sun.rmi.runtime.NewThreadAction;

import java.util.concurrent.TimeUnit;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class GeneralUtils {

     /*
      * Gets the red spawn of specified map
      */
      public static Location getRedSpawn(String map){
        MapDefinition mapDefinition = JSONUtils.getMaps().get(map.toLowerCase());
        return new Location(Bukkit.getWorld(mapDefinition.getWorld()), mapDefinition.getRedX(), mapDefinition.getRedY(), mapDefinition.getRedZ());
      }
    /*
    * Gets the blue spawn of specified map
    */
    public static Location getBlueSpawn(String map) {
        MapDefinition mapDefinition = JSONUtils.getMaps().get(map.toLowerCase());
        return new Location(Bukkit.getWorld(mapDefinition.getWorld()), mapDefinition.getBlueX(), mapDefinition.getBlueY(), mapDefinition.getBlueZ());
    }

    /*
     * Cleans up the futures of our tasks;
     */
    public static void cleanupTasks() {
        PBCraft.getPBCraft().inBetweenGamesFuture.cancel(true);
        PBCraft.getPBCraft().startWarningFuture.cancel(true);
        PBCraft.getPBCraft().gameClassFuture.cancel(true);
    }

    /*
     * Method to schedule our timers
     */
    public static void scheduleTimers() {
        // --Sixty second timer--
        PBCraft.getPBCraft().inBetweenGamesFuture = PBCraft.getPBCraft().scheduler.schedule(PBCraft.getPBCraft().inBetweenGames, 60, TimeUnit.SECONDS);
        PBCraft.getPBCraft().startWarningFuture = PBCraft.getPBCraft().scheduler.schedule(PBCraft.getPBCraft().gameStartWarning, 40, TimeUnit.SECONDS);
    }
    /*
    * Convenience variables
    */
    public static final ItemStack snowBalls = new ItemStack(Material.SNOW_BALL, Material.SNOW_BALL.getMaxStackSize());
    public static final ItemStack egg = new ItemStack(Material.EGG);
    /*
     * Method returning the location of the lobby
     */
    public static Location getLobby() {
        MapDefinition lobbyDefinition = JSONUtils.getMaps().get("lobby");
        int x = lobbyDefinition.getBlueX();
        int y = lobbyDefinition.getBlueY();
        int z = lobbyDefinition.getBlueZ();
        World world = Bukkit.getWorld(lobbyDefinition.getWorld());
        return new Location(world, x, y, z);

    }

    /*
     * Checks if a player is deserving of a new rank after a round
     */
    public static void checkRanks() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            PlayerDBBuffer buffer = PBCraft.getBuffer().get(p.getName());
            if (buffer.getRank() == 1 && buffer.getPoints() > JSONUtils.getConfiguration().getPointsForIronChestPlate()) {
                buffer.setRank(2);
            }
            if (buffer.getRank() == 2 && buffer.getPoints() > JSONUtils.getConfiguration().getPointsForGoldenChestPlate()) {
                buffer.setRank(3);
            }
            if (buffer.getRank() == 3 && buffer.getPoints() > JSONUtils.getConfiguration().getPointsForDiamondChestPlate()) {
                buffer.setRank(4);
            }
            if (buffer.getRank() == 4 &&buffer.getPoints() > JSONUtils.getConfiguration().getPointsForChainMailChestPlate()) {
                buffer.setRank(5);
            }
        }
    }

    /*
     * Switch method taking number rank and returning chestplate
     */
    public static ItemStack getChestPlate(int rank) {
        return RankEnum.getEnum(rank).getChest();
    }

    /*
     * Switch method taking int rank and returning string rank
     */
    public static String getStringPlate(int rank) {
       return  RankEnum.getEnum(rank).getTag();
    }

    /*
     * Gives a player items, i.e snowballs and eggs
     */
    public static void giveItems(Player p) {
        PlayerDBBuffer dbf = PBCraft.getBuffer().get(p.getName());
        p.getInventory().clear();

        // --Give player correct amount of snowballs--
        int limit = (dbf.isDonor()) ? 5 : 3;
        for (int i = 0; i < limit; i++) {
            p.getInventory().addItem(snowBalls);
        }

        //--Give them their chestplate based on rank or donor status
        if(dbf.isDonor()) {
            p.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
        } else {
            p.getInventory().setChestplate(getChestPlate(dbf.getRank()));
        }

        // --Give them additional snowballs they bought
        if (dbf.getSnowballs() != 0) {
            for (int i = 0; i < dbf.getSnowballs(); i++) {
                p.getInventory().addItem(snowBalls);
            }
            dbf.setSnowballs(0);
        }
        // --Give them additional eggs they bought
        if (dbf.getEggs() != 0) {
            for (int i = 0; i < dbf.getEggs(); i++) {
                p.getInventory().addItem(egg);
            }
            dbf.setEggs(0);
        }
        // --Update inventory--
        p.updateInventory();
    }

    /*
     * Sends leaderboard to specified player
     */
    public static void sendLeaderBoard(Player p) {
        ChatUtils.send("Top 10 Leaderboard", p);
        for (String s : PBCraft.getDB().getTop10()) {
            ChatUtils.send("Player " + s + " with " + PBCraft.getDB().getKills(s) + " frags", p);
        }
    }

}
