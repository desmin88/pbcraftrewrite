package net.paintballcraft.plugin;

import net.paintballcraft.plugin.commands.BuyCommand;
import net.paintballcraft.plugin.commands.GeneralCommands;
import net.paintballcraft.plugin.commands.MapCommand;
import net.paintballcraft.plugin.commands.TeamCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class PBCommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String commandString, String[] args) {
        String c = commandString.toLowerCase();

        if(c.equals("red") || c.equals("blue") || c.equals("blue")) {
            return TeamCommand.onCommand(sender, command, commandString.toLowerCase(), args);
        }
        if(c.equals("togglemsg") || c.equals("time") | c.equals("points") || c.equals("leaderboard") || c.equals("vote")) {
            return GeneralCommands.onCommand(sender, command, commandString.toLowerCase(), args);
        }

        if(c.equals("pb")) {
            return MapCommand.onCommand(sender, command, commandString, args);
        }

        if(c.equals("buy")) {
            return BuyCommand.onCommand(sender, command, commandString, args);
        }




        return true;
    }

}
