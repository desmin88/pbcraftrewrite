package net.paintballcraft.plugin.json;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class MapDefinition {
    /**
     * X,Y,Z coordinates for red spawn
     */
    private int redX = 0;
    private int redY = 0;
    private int redZ = 0;

    /**
     * X,Y,Z coordinates for blue spawn
     */
    private int blueX = 0;
    private int blueY = 0;
    private int blueZ = 0;

    /**
     * X,Y,Z coordinates for red spawn
     */
    private String world = "";

   /*
    * Integer storing votes for this particular map
    */
    private int votes = 0;

    /**
     * Getters and Setters
     */
    public int getRedX() {
        return redX;
    }

    public void setRedX(int redX) {
        this.redX = redX;
    }

    public int getRedY() {
        return redY;
    }

    public void setRedY(int redY) {
        this.redY = redY;
    }

    public int getRedZ() {
        return redZ;
    }

    public void setRedZ(int redZ) {
        this.redZ = redZ;
    }

    public int getBlueX() {
        return blueX;
    }

    public void setBlueX(int blueX) {
        this.blueX = blueX;
    }

    public int getBlueY() {
        return blueY;
    }

    public void setBlueY(int blueY) {
        this.blueY = blueY;
    }

    public int getBlueZ() {
        return blueZ;
    }

    public void setBlueZ(int blueZ) {
        this.blueZ = blueZ;
    }

    public String getWorld() {
        return world;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }


}
