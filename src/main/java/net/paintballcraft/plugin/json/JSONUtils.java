package net.paintballcraft.plugin.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import net.paintballcraft.plugin.PBCraft;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class JSONUtils {

    /**
     *  Special Type casing for GSON
     */
    private static Type mapsType = new TypeToken<Map<String, MapDefinition>>() {}.getType();

    /**
     *  GSON instance with pretty printing
     */
    private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

    /**
     *  File objects to our .json files
     */
    private static File configFile = null;
    private static File mapsFile = null;

    /**
     *  POJO object references
     */
    private static Configuration cfg = null;
    private static Map<String, MapDefinition> maps = new HashMap<String, MapDefinition>();

    /**
     *  Getters and Setters for our POJO references
     */
    public static Configuration getConfiguration() {
        return cfg;
    }

    public synchronized static Map<String, MapDefinition> getMaps() {
        return maps;
    }

    /**
     *  Methods for generation and loading
     */
     public static void serverStartup() {
        configFile = new File(PBCraft.getPBCraft().getDataFolder(), "config.json");
        mapsFile = new File(PBCraft.getPBCraft().getDataFolder(), "maps.json");
        if (!PBCraft.getPBCraft().getDataFolder().exists()) {
            firstLaunch();
        } else {
            loadConfiguration();
        }
    }

    public static void firstLaunch() {
        PBCraft.getPBCraft().getDataFolder().mkdir();
        try {
            configFile.createNewFile();
            mapsFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create default data in our pojos.
        maps.put("lobby", new MapDefinition());
        cfg = new Configuration();

        // Write pojo objects to json files
        write(mapsFile, gson.toJson(maps));
        write(configFile, gson.toJson(cfg));
    }

    /**
     *  Saves configuration data - used in persistence
     */
    public synchronized static void saveConfiguration() {
        // Write pojo objects to json files
        write(mapsFile, gson.toJson(maps));
        write(configFile, gson.toJson(cfg));
    }

    /**
     *  Loads configuration if it is not a new server startup
     */
    public static void loadConfiguration() {
        try {
            maps = gson.fromJson(new BufferedReader(new FileReader(mapsFile)), mapsType);
            cfg = gson.fromJson(new BufferedReader(new FileReader(configFile)), Configuration.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Helper method for writing string data to a filer
     */
    public static void write(File file, String content) {
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, false), "UTF8"));
            out.write(content);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
