package net.paintballcraft.plugin.json;

/**
 * @Author William Ryan - desmin88
 * Do not redistribute, modify, or sell in anyway.
 */
public class Configuration {

    /**
     * Database information
     */
    private String dbHostname = "changethis";
    private String dbUsername = "changethis";
    private String dbPassword = "changethis";
    private String dbDataBase = "changethis";

    /**
    * Points given to players on certain events
    */
    private int pointsPerKill = 0;
    private int pointsGivenOnDeath = 0;
    private int pointsForWinningKill = 0;
    private int pointsGivenToWinningTeam = 0;

    /**
    * Points required for player rankups
    */
    private int pointsForIronChestPlate = 1;
    private int pointsForGoldenChestPlate = 2;
    private int pointsForDiamondChestPlate = 3;
    private int pointsForChainMailChestPlate = 4;

    /**
    * Points required to buy items in the in-game shop
    */
    private int priceForSnowBallStack = 10;

    private int priceForSpeed1M = 10;
    private int priceForSpeed3M = 25;
    private int priceForSpeed5M = 50;

    private int priceForJump1M = 10;
    private int priceForJump3M = 25;
    private int priceForJump5M = 50;

    private int pricePerEgg = 50;

    /**
     * Getters and Setters
    */
    public String getDbHostname() {
        return dbHostname;
    }

    public String getDbUsername() {
        return dbUsername;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public String getDbDataBase() {
        return dbDataBase;
    }

    public int getPointsPerKill() {
        return pointsPerKill;
    }

    public int getPointsGivenOnDeath() {
        return pointsGivenOnDeath;
    }

    public int getPointsForWinningKill() {
        return pointsForWinningKill;
    }

    public int getPointsGivenToWinningTeam() {
        return pointsGivenToWinningTeam;
    }

    public int getPointsForIronChestPlate() {
        return pointsForIronChestPlate;
    }

    public int getPointsForGoldenChestPlate() {
        return pointsForGoldenChestPlate;
    }

    public int getPointsForDiamondChestPlate() {
        return pointsForDiamondChestPlate;
    }

    public int getPointsForChainMailChestPlate() {
        return pointsForChainMailChestPlate;
    }

    public int getPriceForSnowBallStack() {
        return priceForSnowBallStack;
    }

    public int getPriceForSpeed1M() {
        return priceForSpeed1M;
    }

    public int getPriceForSpeed3M() {
        return priceForSpeed3M;
    }

    public int getPriceForSpeed5M() {
        return priceForSpeed5M;
    }

    public int getPriceForJump1M() {
        return priceForJump1M;
    }

    public int getPriceForJump3M() {
        return priceForJump3M;
    }

    public int getPriceForJump5M() {
        return priceForJump5M;
    }

    public int getPricePerEgg() {
        return pricePerEgg;
    }
}
